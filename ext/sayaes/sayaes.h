/*//////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
// Sayaes
//
//////////////////////////////////////////////////////////////////////////////*/
#ifndef __sayaes_h
#define __sayaes_h

/* structure to store our key and keysize */
typedef struct {
  char key[256];  /* max key is 256 */
  int  key_bits;  /* 128, 192, 256  */

  /* Encryption Round Keys */
  unsigned int erk[64];
  unsigned int initial_erk[64];

  /* Decryption Round Keys */
  unsigned int drk[64];
  unsigned int initial_drk[64];

  /* Number of rounds. */
  int nr;

  /* enciphering set */
  uint64_t l, mask, hmask;
  uint8_t w, h, r; // w up to 240, h upto 120.  r is upto 16, indeed 4 bits but we keep 8
} sayaes_t;

/* functions */
void Init_sayaes();
VALUE sayaes_alloc(VALUE klass);
VALUE sayaes_initialize(VALUE self, VALUE key);
void sayaes_gen_tables();
int  sayaes_initialize_state(sayaes_t* sayaes_config);
VALUE sayaes_key(VALUE self);

/* garbage collection */
void sayaes_mark(sayaes_t* sayaes_config);
void sayaes_free(sayaes_t* sayaes_config);
void sayaes_module_shutdown(sayaes_t* sayaes_config);    

/* and actual, bonafide encryption */
VALUE sayaes_encrypt(VALUE self, VALUE buffer);
VALUE sayaes_decrypt(VALUE self, VALUE buffer);

void sayaes_encrypt_block(sayaes_t* sayaes, unsigned char input[16], unsigned char output[16]);
void sayaes_decrypt_block(sayaes_t* sayaes, unsigned char input[16], unsigned char output[16]);

int sayaes_reinitialize_state();
int sayaes_initialize_state();

/*Serependity*/
/*Generate n numbers starting from x */
VALUE sayaes_init_feistel_ext(VALUE self, VALUE rb_r, VALUE rb_l);
VALUE sayaes_r(VALUE self);
VALUE sayaes_l(VALUE self);
VALUE sayaes_w(VALUE self);

// gives the log2 for a 64 bit number
#define LOG2(X) ((uint8_t) (8*sizeof(uint64_t) - __builtin_clzll((X))))
VALUE sayaes_encipher(VALUE self, VALUE rb_x);
VALUE sayaes_decipher(VALUE self, VALUE rb_y);

uint64_t sayaes_round_e(sayaes_t* sayaes, uint64_t x, uint8_t j);
uint64_t sayaes_round_d(sayaes_t* sayaes, uint64_t x, uint8_t j);
uint64_t sayaes_prf(sayaes_t* sayaes, uint64_t x, uint8_t j);
VALUE sayaes_prf_big(VALUE self, VALUE hxj);

#endif // __sayaes_h

