Sayaes 
----
Sayaes is a ruby gem for AES-based format preserving encryption.
In cryptography, format-preserving encryption (FPE), refers to encrypting in such a way that the output (the ciphertext) is in the same format as the input (the plaintext).
https://en.wikipedia.org/wiki/Format-preserving_encryption

You may use it to generate:
- millions of large unique sets of numbers fast and efficiently.
- discount, promotion coupon codes
- format preserving encrypted ciphertexts


It provides: 
- AES encryption
- AES based format preserving encryption/decryption implementation (FPE)
- Unique code building functions such as coupon codes
- Profanity library to filter out codes
-
## Story
Years ago I took a Cryptography I course from Dan Boneh of Stanford on Coursera. 
The instructor was very knowledgeable and concise. 
I have built an AES based ruby gem for generating large numbers of unique codes.
Read the [medium story](https://medium.com/@alperakgun/what-open-sourcing-my-side-project-taught-me-7f19324a0a6a):  

Note: Please use at your own risk.


## Installation

Sayaes is a ruby gem library with C extensions. Install using rubygems on ruby 2.4+

`gem install sayaes`

## Usage

### Load and get ready
```
require 'sayaes'
require 'securerandom'

SECURE_KEY = SecureRandom.random_bytes(16) # 16 byte random key
sayaes = Sayaes.new(SECURE_KEY);
```

### AES encryption
```
plain_text = 'Hello, World!'
cipher_text = sayaes.encrypt(plain_text)
decrypted_text = sayaes.decrypt(cipher_text)

puts plain_text, cipher_text, decrypted_text
```

### Format preserving encryption

Choose a set size, and simply find encipher indexes for your format

```
set_size = 10**8 - 1  # a large enough set_size
sayaes.init_feistel set_size

# encipher and decipher one value
my_credit_card =  'xxxx-xxxx-4242-4242'

ciphered_value = sayaes.encipher(42424242)
ciphered_card = "xxxx-xxxx-#{ciphered_value.to_s[0..3]}-#{ciphered_value.to_s[4..7]}"
....
sayaes.decipher(ciphered_value) # => voila!


# encipher and decipher a range
cipher_array = sayaes.encipher_range(7, 10)
cipher_array.each_with_index { |e, i| sayaes.decipher(e) == 7 + i }

```

### Coupon code generation
```
sayaes.init_custom_template('\G\I\T\L\A\B-ww-WW-ZZZZ-00-\J\U\L\Y', filter_out_repeating: true, filter_out_badword: true)
sayaes.encode_range(1, 10)
```

Template alphabets
```
    'W' => %w[A B C D E F G H I J K L M N O P Q R S T U V W X Y Z],
    'w' => %w[a b c d e f g h i j k l m n o p q r s t u v w x y z],
    'X' => %w[A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
              0 1 2 3 4 5 6 7 8 9],
    'x' => %w[a b c d e f g h i j k l m n o p q r s t u v w x y z
              0 1 2 3 4 5 6 7 8 9],
    'Y' => %w[A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
              a b c d e f g h i j k l m n o p q r s t u v w x y z],
    'Z' => %w[A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
              a b c d e f g h i j k l m n o p q r s t u v w x y z
              0 1 2 3 4 5 6 7 8 9],
    # Safe sets: omit chars confusable in printing 0/O/o/1/l/
    'P' => %w[A B C D E F G H J K L M N P Q R S T U V W X Y Z
              2 3 4 5 6 7 8 9],
    'p' => %w[a b c d e f g h j k l m n p q r s t u v w x y z
              2 3 4 5 6 7 8 9],
    'Q' => %w[A B C D E F G H J K L M N P Q R S T U V W X Y Z
              a b c d e f g h j k m n p q r s t u v w x y z
              2 3 4 5 6 7 8 9],
    '0' => %w[0 1 2 3 4 5 6 7 8 9],
    '1' => %w[1 2 3 4 5 6 7 8 9],
    '2' => %w[2 3 4 5 6 7 8 9]

```

# Contributing

Contributions, and merge requests are welcome. See CONTRIBUTING.md  file





