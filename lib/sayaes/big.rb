# frozen_string_literal: true

# class Sayaes
#  Includes implementation for big numbers
class Sayaes
  attr_accessor :r_big, :l_big, :w_big, :h_big, :hmask_big, :mask_big, :mode

  def init_feistel(l, r = 12)
    @l_big = l
    @r_big = r
    @w_big = feistel_size(l)
    @h_big = @w_big >> 1
    @mask_big = (1 << @w_big) - 1
    @hmask_big = (1 << @h_big) - 1

    # @mode, and @max symbols for auto
    if @w_big <= 64
      # prf can do 32bits, number can be x2 so 64bit max uint64_t
      @mode = :c # max 10 letters
      init_feistel_ext(l, r) # init the c extension
    elsif @w_big > 64 && @w_big <= 192
      # prf can do 15bytes=120bits x2 => 240bits but
      # to be safe on rb_big_pack quirks, 192 for mixed
      @mode = :mixed # max 32 letters
    else # slowest, bignum but can do anything
      @mode = :ruby # any..
    end
  end

  # l here is indeed the max value in the set
  def feistel_size(l)
    # (Math.log(l+1,2).ceil.to_i + 1) & ~1
    # doesn't work due to math.log fload precision
    (l.to_s(2).length + 1) & ~1
  end

  def encipher_big(y)
    if y > @l_big
      raise(ArgumentError,
            "number can't be larger than the set size")
    end

    begin
      @r_big.times { |j| y = round_e_big(y, j) }
    end while y > @l_big
    y
  end

  def decipher_big(y)
    if y > @l_big
      raise(ArgumentError,
            "number can't be larger than the set size")
    end

    begin
      @r_big.times { |j| y = round_d_big(y, @r_big - 1 - j) }
    end while y > @l_big
    y
  end

  def round_e_big(x, j)
    right = x & @hmask_big
    ((right << @h_big) | ((x >> @h_big) ^ (prf_big_ruby((right << 8) | j) &
                                           @hmask_big))) & @mask_big
  end

  def round_d_big(x, j)
    left = x >> @h_big
    ((((x & @hmask_big) ^ (prf_big_ruby((left << 8) | j) &
                           @hmask_big)) << @h_big) | left) & @mask_big
  end

  def prf_big_ruby(hxj)
    if @mode == :ruby
      pt = [format('%02x', hxj)].pack('H*')
      encrypt(pt).unpack1('H*').to_i(16) & @hmask_big
    else
      prf_big(hxj)
    end
  end
end
