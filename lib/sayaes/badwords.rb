# frozen_string_literal: true

# class Sayaes
#  Includes badwords and profanity implementation
class Sayaes
  class << self
    attr_accessor :badwords
  end

  # bad word detection
  @@badwords = { en: %w[ass bastard beast bestial bitch blowjob
                        bullshit clit cock cum boner cunt damn dildo
                        dick dink ejacu fag fart fist fuck fuk bang
                        sex hell horny jism jiz kock kondum kum
                        lust lusting mothafuck nig orgasm phuk pis
                        porn prick puss shit slut smut spunk twat] }
  def self.profane?(tekst, language)
    tekst_down = tekst.downcase
    @@badwords[language].any? { |bw| tekst_down.include?(bw) }
  end
end
