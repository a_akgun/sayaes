# frozen_string_literal: true

# class Sayaes
#  Includes aes, fpe and coupon code implementation
class Sayaes
  attr_accessor :user_alphabets, :custom_template, :max_possible_codes

  # from template create encoding tables
  def init_custom_template(custom_template, options = {})
    # template contains an alphabet key or an \ escaped char
    @custom_template = custom_template
    @cells = @custom_template.scan(/[\\]?./)
    @user_alphabets ||= {}
    @user_alphabets.merge!(@@alphabets)
    @cells_w_alphabets = @cells.map do |c|
      @user_alphabets[c] || [c.gsub('\\', '')]
    end
    @cells_w_sizes = @cells_w_alphabets.map(&:length)
    @cells_w_index_powers = @cells_w_sizes.map.with_index do |_cs, i|
      @cells_w_sizes[i + 1..-1].reduce(1) { |mul, e| mul * e }
    end
    @max_possible_codes = @cells_w_sizes.reduce(1) do |mul, e|
      mul * e
    end
    options[:rounds] ||= 12
    init_feistel max_possible_codes, options[:rounds]
  end

  # return an array of integers for a range
  def encipher_range(x, n, _options = {})
    [*x..(x + n - 1)].map! do |j|
      @mode == :c ? encipher(j) : encipher_big(j)
    end
  end

  # return an array of templated strings
  def encode_range(x, n, options = {})
    [*x..(x + n - 1)].map! do |j|
      m = @mode == :c ? encipher(j) : encipher_big(j)
      encip = @cells_w_index_powers.map.with_index do |cip, jj|
        d, m = m.divmod(cip)
        @cells_w_alphabets[jj][d]
      end.join
      if options[:filter_out_repeating] && encip =~ /(\w)(\1+)(\1+)(\1+)/
        encip = nil
      elsif options[:filter_out_badword] && Sayaes.profane?(encip, :en)
        encip = nil
      end
      encip
    end.compact
  end

  # enciphers an array of integers to a file
  def encipher_range_to_file(x, n, filename, _options = {})
    out = File.new(filename, 'w')
    (x..(x + n - 1)).each do |j|
      out.puts(@mode == :c ? encipher(j) : encipher_big(j))
    end
    out.close
    n
  end

  # enciphers up to n codes to file
  # badwords and repeating ones go to their own files
  def encode_range_to_file(x, n, filename, options = {})
    out = File.new(filename, 'w')
    out_repeating = File.new(filename + '_repeating', 'w')
    out_badword = File.new(filename + '_badword', 'w')
    n_success = 0
    n_badword = 0
    n_repeating = 0
    j = x
    while n_success < n
      m = @mode == :c ? encipher(j) : encipher_big(j)
      encip = @cells_w_index_powers.map.with_index do |cip, jj|
        d, m = m.divmod(cip)
        @cells_w_alphabets[jj][d]
      end.join
      if options[:filter_out_repeating] && encip =~ /(\w)(\1+)(\1+)(\1+)/
        out_repeating.puts(encip)
        n_repeating += 1
      elsif options[:filter_out_badword] && Sayaes.profane?(encip, :en)
        out_badword.puts(encip)
        n_badword += 1
      else
        out.puts(encip)
        n_success += 1
      end
      j += 1
    end
    out_repeating.close
    out_badword.close
    out.close
    [n_success, n_badword, n_repeating, j - x]
  end

  # predefined alphabets user alphabets add to: 'A', 'B', 'C', 'D', 'E'
  @@alphabets = {
    'W' => %w[A B C D E F G H I J K L M N O P Q R S T U V W X Y Z],
    'w' => %w[a b c d e f g h i j k l m n o p q r s t u v w x y z],
    'X' => %w[A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
              0 1 2 3 4 5 6 7 8 9],
    'x' => %w[a b c d e f g h i j k l m n o p q r s t u v w x y z
              0 1 2 3 4 5 6 7 8 9],
    'Y' => %w[A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
              a b c d e f g h i j k l m n o p q r s t u v w x y z],
    'Z' => %w[A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
              a b c d e f g h i j k l m n o p q r s t u v w x y z
              0 1 2 3 4 5 6 7 8 9],
    # Safe sets: omit chars confusable in printing 0/O/o/1/l/
    'P' => %w[A B C D E F G H J K L M N P Q R S T U V W X Y Z
              2 3 4 5 6 7 8 9],
    'p' => %w[a b c d e f g h j k l m n p q r s t u v w x y z
              2 3 4 5 6 7 8 9],
    'Q' => %w[A B C D E F G H J K L M N P Q R S T U V W X Y Z
              a b c d e f g h j k m n p q r s t u v w x y z
              2 3 4 5 6 7 8 9],
    '0' => %w[0 1 2 3 4 5 6 7 8 9],
    '1' => %w[1 2 3 4 5 6 7 8 9],
    '2' => %w[2 3 4 5 6 7 8 9]
  }
end
