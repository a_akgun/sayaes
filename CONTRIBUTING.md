== Contributing

Contributions, merge requests, and forks are welcome.
Sayaes is available on Gitlab source code management and DevOps platform.

https://gitlab.com/say-group/sayaes

Sayaes is a ruby gem with C extension. You must install build tools.

```
# Install ruby build tools on ubuntu 18.04
sudo apt install autoconf bison build-essential

cd ext/sayaes/
ruby extconf.rb
make

```


=== Testing
In the root sayaes folder

```
rake test
```