# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name = 'sayaes'
  s.version = '1.1.1'
  s.summary = 'Sayaes AES based format preserving encryption/decryption'
  s.description = s.summary + '.'
  s.files = Dir['ext/**/*.{rb,c,h}'] + Dir['lib/**/*.rb'] + Dir['spec/**/*.rb']
  s.extensions << 'ext/sayaes/extconf.rb'
  s.require_paths = %w[lib ext]
  s.has_rdoc = true
  s.rubyforge_project = 'sayaes'
  s.extra_rdoc_files = Dir['[A-Z]*']
  s.rdoc_options << '--title' <<  'Sayaes uses aes for fpe, coupon code gen'
  s.author = 'Alper'
  s.email = 'alperakgun@gmail.com'
  s.homepage = 'https://gitlab.com/say-group/sayaes'
end
