require 'minitest/pride'
require 'minitest/benchmark'
require 'minitest/autorun'

$LOAD_PATH.unshift "#{File.dirname(__FILE__)}/../ext/sayaes/#{RUBY_PLATFORM}"
Dir.chdir("#{File.dirname(__FILE__)}/../ext/sayaes") do
  system 'ruby extconf.rb'
  system 'make clean'
  system "make && make install RUBYARCHDIR=#{RUBY_PLATFORM}"
end

require 'sayaes'

describe 'Sayaes' do
  describe 'when encrypting and decrypting back' do
    it 'must return the original text' do
      # key can be 128, 192, or 256 bits
      key = '42#3b%c$dxyT,7a5=+5fUI3fa7352&^:'
      aes = Sayaes.new(key)
      text = 'Hey there, how are you?'
      data = aes.encrypt(text)
      aes.decrypt(data).must_equal text # "Hey there, how are you?"
    end
  end

  describe 'when given keys' do
    it 'should accept 128, 192, and 256-bit keys' do
      [128, 192, 256].each do |bits|
        key = 'a' * (bits / 8)
        aes = Sayaes.new(key)
        aes.key.must_equal key
      end
    end
  end

  describe 'when key is not in range supported' do
    it "should raise an exception is the key isn't in the accepted range" do
      -> { Sayaes.new('key') }.must_raise ArgumentError # 24-bit
      -> { Sayaes.new('keykeyke') }.must_raise ArgumentError # 64-bit
      -> { Sayaes.new('keykeykeykeykeykeykeykeykeykeykey') }.must_raise ArgumentError # 264-bit
    end
  end

  describe 'when given a lot of text to encrypt and decrypt back' do
    it 'should encrypt and decrypt messages' do
      phrases = [
        'Hey there, how are you?',
        'You know, encryption is just so gosh-darned cool I wish I had a girlfriend to tell about it!!',
        'A subject for a great poet would be God\'s boredom after the seventh day of creation.',
        'The individual has always had to struggle to keep from being overwhelmed by the tribe. If you try it, ' \
          'you will be lonely often, and sometimes frightened. But no price is too high to pay for the privilege of owning yourself.',
        'But although all our knowledge begins with experience, it does not follow that it arises from experience.',
        'Some with nulls\x00in-between\x00\x00 you know'
      ]

      phrases.each do |text|
        [128, 192, 256].each do |bits|
          key = 'a' * (bits / 8)
          aes = Sayaes.new(key)
          aes.key.must_equal key
          data = aes.encrypt(text)
          aes.decrypt(data).must_equal text
        end
      end
    end
  end

  describe 'when given different key lengths' do
    it 'should have a different encryptions  based on the key length' do
      aes1 = Sayaes.new '12345678901234567890123456789012'
      aes2 = Sayaes.new '123456789012345678901234'
      aes3 = Sayaes.new '1234567890123456'
      text  = 'So long and thanks for all the fish'
      secret1 = aes1.encrypt(text)
      secret2 = aes2.encrypt(text)
      secret3 = aes3.encrypt(text)
      [secret1, secret2, secret3].each do |secret|
        ([secret1, secret2, secret3] - [secret]).each { |message| message.wont_equal secret }
      end
    end
  end
  describe 'when i encrypt all members of a set of 140' do
    it 'should return unique permutation' do
      aes = Sayaes.new 'b' * 16
      set_size = 140
      aes.init_feistel set_size - 1
      set = {}
      set_size.times do |i|
        set[i] = aes.encipher(i)
      end
      set.keys.uniq.length.must_equal 140
      set.values.uniq.length.must_equal 140
    end
  end

  describe 'when i give predefined sets' do
    it 'should raise argument error' do
      aes = Sayaes.new 'b' * 16
      aes.init_feistel 140 - 1
      aes.encipher(0).must_equal 41
      aes.encipher(7).must_equal 117
      aes.encipher(139).must_equal 87
    end
  end

  describe 'when i give larger or equal input than set size' do
    it 'should raise argument error' do
      aes = Sayaes.new 'b' * 16
      aes.init_feistel 140 - 1
      -> { aes.encipher(140) }.must_raise ArgumentError
      -> { aes.encipher(141) }.must_raise ArgumentError
    end
  end

  describe 'when i encrypt and decrypt a set' do
    it 'should return the same value' do
      aes = Sayaes.new 'c' * 16
      set_size = 140
      aes.init_feistel set_size - 1
      set = {}
      set_size.times do |i|
        ct = aes.encipher(i)
        pt = aes.decipher(ct)
        set[i] = [ct, pt]
        i.must_equal pt
      end
    end
  end

  describe 'when i give a bignum' do
    it 'should raise a range error' do
      aes = Sayaes.new 'b' * 16
      -> { aes.init_feistel_ext(10**20 - 1, 12) }.must_raise RangeError
    end
  end

  describe 'when i encrypt 140, 61b,62b, 63b, 64b integers' do
    it 'should work properly' do
      aes = Sayaes.new 'c' * 16
      [140, 0x1 << 61, 0x1 << 62, 0x1 << 63, 0x1 << 64].each do |set_size|
        aes.init_feistel set_size - 1
        aes.decipher(aes.encipher(7)).must_equal 7
      end
    end
  end

  describe 'when i ask for feistel sizes' do
    it 'should work properly' do
      aes = Sayaes.new 'c' * 16
      aes.feistel_size(15).must_equal 4
      aes.feistel_size(540).must_equal 10
      aes.feistel_size(1023).must_equal 10
      aes.feistel_size(1024).must_equal 12
      aes.feistel_size(1025).must_equal 12
      aes.feistel_size((1 << 63) - 1).must_equal 64
      aes.feistel_size(1 << 63).must_equal 64
      aes.feistel_size((1 << 64) - 1).must_equal 64
      aes.feistel_size((1 << 64)).must_equal 66
      aes.feistel_size((1 << 128) - 1).must_equal 128
      aes.feistel_size((1 << 128)).must_equal 130
      aes.feistel_size((1 << 128) + 1).must_equal 130
    end
  end

  describe 'when i ask for profanity' do
    it 'should return true or false' do
      Sayaes.profane?('ass', :en).must_equal true
      Sayaes.profane?('21aSS', :en).must_equal true
      Sayaes.profane?('11shIt2178', :en).must_equal true
      Sayaes.profane?('sht8', :en).must_equal false
    end
  end

  describe 'when i give a range' do
    it 'should return enciphers for the range' do
      aes = Sayaes.new 'c' * 16
      aes.init_feistel 1400 - 1
      arr = aes.encipher_range(7, 1000)
      arr.each_with_index { |e, i| aes.decipher(e).must_equal 7 + i }
    end
  end

  describe 'when i encode range' do
    it 'should return encodings' do
      aes = Sayaes.new 'c' * 16
      aes.init_custom_template('ZZZZ-ZZ')
      arr = aes.encode_range(0, 100)
      arr.length.must_equal 100
    end
  end

  describe 'when i ask to write a range to a file' do
    it 'should return enciphers for the range' do
      aes = Sayaes.new 'j' * 16
      aes.init_feistel 15_000_000
      fname = Dir.tmpdir + '/' + make_tmpname('enc', '.txt')
      aes.encipher_range_to_file(0, 10_000, fname)
      `wc -l #{fname}`.split[0].to_i.must_equal 10_000
    end
  end

  describe 'when i give 100K with custom templates, non-repeating, profanity' do
    it 'should return enciphers for the range' do
      aes = Sayaes.new 'd' * 16
      aes.init_custom_template('\A\sWWWWXY012-\Z\0') # small set
      fname = Dir.tmpdir + '/' + make_tmpname('enc', '.txt')
      n_success, n_badword, n_repeating, j = aes.encode_range_to_file(0, 10_000, fname, filter_out_repeating: true, filter_out_badword: true)
      (n_success + n_badword + n_repeating).must_equal j
      n_success.must_equal 10_000
      (n_success + n_badword + n_repeating).must_equal j
      `wc -l #{fname}`.split[0].to_i.must_equal n_success
      `wc -l #{fname}_badword`.split[0].to_i.must_equal n_badword
      `wc -l #{fname}_repeating`.split[0].to_i.must_equal n_repeating
    end
  end

  describe 'when i init Sayaes 10000 times with securerandom' do
    it 'should init fine' do
      10_000.times do
        aes = Sayaes.new(SecureRandom.random_bytes(16))
        aes = Sayaes.new(Base64.decode64(SecureRandom.base64(16)))
      end
    end
  end

  describe 'when i call prf_big' do
    it 'should work fine' do
      aes = Sayaes.new(SecureRandom.random_bytes(16))
      aes.prf_big(42)
    end
  end
end

require 'securerandom'
require 'base64'
