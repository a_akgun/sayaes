require 'tempfile'

def make_tmpname(s, e)
  "#{s}-#{Time.now.strftime('%Y%m%d')}-#{rand(0x100000000).to_s(36)}#{e}"
end
