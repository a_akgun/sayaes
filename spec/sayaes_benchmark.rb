require 'minitest/pride'
require 'minitest/benchmark'
require 'minitest/autorun'

require 'sayaes'

describe 'SayBenchmark' do
  before do
    puts ' '
  end
  bench_range { [1000, 2000, 10_000] }
  if ENV['BENCH'] == 'BIG' || ENV['BENCH'] == 'ALL'
    bench_performance_linear 'enc 8r setx1', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel(((1 << 65) - 1), 8)
      n.times { |x| aes.encipher_big(x) }
    end
    bench_performance_linear 'enc 12r setx1', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel((1 << 65) - 1)
      n.times { |x| aes.encipher_big(x) }
    end
    bench_performance_linear 'enc 12r setx4', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel((1 << 65) - 1)
      n.times { |x| aes.encipher_big(x) }
    end
    bench_performance_linear 'enc 12r setx2', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel((1 << 65) - 1)
      n.times { |x| aes.encipher_big(x) }
    end

    bench_performance_linear 'enc 12r setx1 190bit', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel((1 << 190) - 1)
      n.times { |x| aes.encipher_big(x) }
    end

    bench_performance_linear 'enc 12r div1000', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel((1 << 65) - 1)
      (n / 1000).times { |x| aes.encipher_range(x * 1000, 1000, use_big: true) }
    end
  end
  if ENV['BENCH'] == 'C' || ENV['BENCH'] == 'ALL'
    bench_performance_linear 'enc 8r setx1', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel(1_000_000, 8)
      n.times { |x| aes.encipher(x) }
    end
    bench_performance_linear 'enc 12r setx1', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel 1_000_000
      n.times { |x| aes.encipher(x) }
    end
    bench_performance_linear 'enc 12r setx4', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel 1_100_000
      n.times { |x| aes.encipher(x) }
    end
    bench_performance_linear 'enc 12r setx2', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel 2_100_000
      n.times { |x| aes.encipher(x) }
    end

    bench_performance_linear 'enc 12r setx1 64bit', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel((1 << 64) - 1)
      n.times { |x| aes.encipher(x) }
    end

    bench_performance_linear 'enc 12r div1000', 0.90 do |n|
      aes = Sayaes.new 'c' * 16
      aes.init_feistel(1_000_000)
      (n / 1000).times { |x| aes.encipher_range(x * 1000, 1000) }
    end
  end
  if ENV['BENCH'] == 'MIN' || ENV['BENCH'] == 'ALL'
    bench_performance_linear '3Xenc 12r setx2 badword non-repeat to file', 0.90 do |n|
      aes = Sayaes.new('d' * 16)
      aes.init_custom_template('Z' * 3)
      fname = Dir.tmpdir + '/' + Dir::Tmpname.make_tmpname(['enc', '.txt'], nil)
      aes.encode_range_to_file(0, n, fname, filter_out_repeating: true, filter_out_badword: true)
    end
    bench_performance_linear '10Xenc 12r setx2 badword non-repeat to file', 0.90 do |n|
      aes = Sayaes.new('d' * 16)
      aes.init_custom_template('Z' * 10)
      fname = Dir.tmpdir + '/' + Dir::Tmpname.make_tmpname(['enc', '.txt'], nil)
      aes.encode_range_to_file(0, n, fname, use_template: true, filter_out_repeating: true, filter_out_badword: true)
    end
    bench_performance_linear '10XBIGN!enc 12r setx2 badword non-repeat to file', 0.90 do |n|
      aes = Sayaes.new('d' * 16)
      aes.init_custom_template('Z' * 10)
      fname = Dir.tmpdir + '/' + Dir::Tmpname.make_tmpname(['enc', '.txt'], nil)
      aes.encode_range_to_file(aes.l_big / 2, n, fname, filter_out_repeating: true, filter_out_badword: true)
    end
    bench_performance_linear '10XMixed!enc 12r setx2 badword non-repeat to file', 0.90 do |n|
      aes = Sayaes.new('d' * 16)
      aes.init_custom_template('Z' * 10)
      aes.mode = :mixed
      fname = Dir.tmpdir + '/' + Dir::Tmpname.make_tmpname(['enc', '.txt'], nil)
      aes.encode_range_to_file(0, n, fname, filter_out_repeating: true, filter_out_badword: true)
    end
    bench_performance_linear '10XRuby!enc 12r setx2 badword non-repeat to file', 0.90 do |n|
      aes = Sayaes.new('d' * 16)
      aes.init_custom_template('Z' * 10)
      aes.mode = :ruby
      fname = Dir.tmpdir + '/' + Dir::Tmpname.make_tmpname(['enc', '.txt'], nil)
      aes.encode_range_to_file(0, n, fname, filter_out_repeating: true, filter_out_badword: true)
    end
    bench_performance_linear '11Xenc 12r setx2 badword non-repeat to file', 0.90 do |n|
      aes = Sayaes.new('d' * 16)
      aes.init_custom_template('Z' * 11)
      fname = Dir.tmpdir + '/' + Dir::Tmpname.make_tmpname(['enc', '.txt'], nil)
      aes.encode_range_to_file(0, n, fname, filter_out_repeating: true, filter_out_badword: true)
    end
    bench_performance_linear '32Xenc 12r setx2 badword non-repeat to file', 0.90 do |n|
      aes = Sayaes.new('d' * 16)
      aes.init_custom_template('Z' * 32)
      fname = Dir.tmpdir + '/' + Dir::Tmpname.make_tmpname(['enc', '.txt'], nil)
      aes.encode_range_to_file(0, n, fname, filter_out_repeating: true, filter_out_badword: true)
    end
    bench_performance_linear '32XBIGN!enc 12r setx2 badword non-repeat to file', 0.90 do |n|
      aes = Sayaes.new('d' * 16)
      aes.init_custom_template('Z' * 32)
      fname = Dir.tmpdir + '/' + Dir::Tmpname.make_tmpname(['enc', '.txt'], nil)
      aes.encode_range_to_file(aes.l_big / 2, n, fname, filter_out_repeating: true, filter_out_badword: true)
    end
    bench_performance_linear '33Xenc 12r setx2 badword non-repeat to file', 0.90 do |n|
      aes = Sayaes.new('d' * 16)
      aes.init_custom_template('Z' * 33)
      fname = Dir.tmpdir + '/' + Dir::Tmpname.make_tmpname(['enc', '.txt'], nil)
      aes.encode_range_to_file(0, n, fname, filter_out_repeating: true, filter_out_badword: true)
    end
    bench_performance_linear '190Xenc 12r setx2 badword non-repeat to file', 0.90 do |n|
      aes = Sayaes.new('d' * 16)
      aes.init_custom_template('Z' * 190)
      fname = Dir.tmpdir + '/' + Dir::Tmpname.make_tmpname(['enc', '.txt'], nil)
      aes.encode_range_to_file(0, n, fname, filter_out_repeating: true, filter_out_badword: true)
    end
  end
end
