require 'minitest/pride'
require 'minitest/benchmark'
require 'minitest/autorun'

require 'sayaes'

describe 'Sayaes' do
  describe 'when i encrypt all members of a set of 140' do
    it 'should return unique permutation' do
      aes = Sayaes.new 'b' * 16
      set_size = 140
      aes.init_feistel set_size - 1
      set = {}
      set_size.times do |i|
        set[i] = aes.encipher_big(i)
      end
      set.keys.uniq.length.must_equal set.values.uniq.length
    end
  end

  describe 'when i give larger or equal input than set size' do
    it 'should raise argument error' do
      aes = Sayaes.new 'b' * 16
      aes.init_feistel 140 - 1
      -> { aes.encipher_big(140) }.must_raise ArgumentError
      -> { aes.encipher_big(141) }.must_raise ArgumentError
    end
  end

  describe 'when i encrypt and decrypt a set' do
    it 'should return the same value' do
      aes = Sayaes.new 'c' * 16
      set_size = 140
      aes.init_feistel set_size - 1
      set = {}
      set_size.times do |i|
        ct = aes.encipher_big(i)
        pt = aes.decipher_big(ct)
        set[i] = [ct, pt]
        i.must_equal pt
      end
    end
  end

  describe 'when i encrypt 140, 61b,62b, 63b, 64b, 128b, 256b integers' do
    it 'should work properly' do
      aes = Sayaes.new 'c' * 16
      [140, 0x1 << 61, 0x1 << 62, 0x1 << 63, 0x1 << 64, 1 << 128, 1 << 256, 1 << 1000].each do |set_size|
        aes.init_feistel set_size - 1
        aes.decipher_big(aes.encipher_big(7)).must_equal 7
      end
    end
  end

  describe 'when i give a range' do
    it 'should return encipher_bigs for the range' do
      aes = Sayaes.new 'c' * 16
      aes.init_feistel((1 << 65) - 1)
      # automatic big chosen?
      arr = aes.encipher_range(7, 1000)
      arr.each_with_index { |e, i| aes.decipher_big(e).must_equal 7 + i }
    end
  end

  describe 'when i ask to write a range to a file' do
    it 'should return enciphers for the range' do
      aes = Sayaes.new 'j' * 16
      aes.init_feistel((1 << 65) - 1)
      fname = Dir.tmpdir + '/' + make_tmpname('enc', '.txt')
      aes.encipher_range_to_file(0, 10_000, fname)
      `wc -l #{fname}`.split[0].to_i.must_equal 10_000
    end
  end

  describe 'when i give 100K with custom templates, non-repeating, profanity' do
    it 'should return encipher_bigs for the range' do
      aes = Sayaes.new 'd' * 16
      aes.init_custom_template('\A\sWWWWXY012-\Z\0ZZZZZZZZZZ') # mode must be big
      fname = Dir.tmpdir + '/' + make_tmpname('enc', '.txt')
      n_success, n_badword, n_repeating, j = aes.encode_range_to_file(0, 10_000, fname, filter_out_repeating: true, filter_out_badword: true)
      (n_success + n_badword + n_repeating).must_equal j
      n_success.must_equal 10_000
      `wc -l #{fname}`.split[0].to_i.must_equal n_success
      `wc -l #{fname}_badword`.split[0].to_i.must_equal n_badword
      `wc -l #{fname}_repeating`.split[0].to_i.must_equal n_repeating
    end
  end
end
